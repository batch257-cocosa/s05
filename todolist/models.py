from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoItem(models.Model):
	# CharField = String value
	# DateTimeField = Datetime datatype
	task_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "Pending")
	date_created = models.DateTimeField("Date Created")
	# Adding a user_i to the ToDOItem table, which is a foreign key connected to Users table
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


class ToDoEvent(models.Model):
	event_name = models.CharField(max_length=50)
	description = models.CharField(max_length=200)
	status = models.CharField(max_length=50, default="Pending")
	event_date = models.DateField("Event Date")
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")